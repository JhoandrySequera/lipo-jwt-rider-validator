package com.liporide.jwtRiderValidator.util;

public enum FirebaseCredential {

    DEVELOP {
        @Override public String environmentName() { return "develop"; }

        @Override
        public String dataBaseUrl() {
            return "https://cabbie-passenger-debug.firebaseio.com";
        }

        @Override
        public String credential() {
            return "{\n" +
                    "  \"type\": \"service_account\",\n" +
                    "  \"project_id\": \"cabbie-passenger-debug\",\n" +
                    "  \"private_key_id\": \"79be50c240e944e6453c34e9629aa4427c70626a\",\n" +
                    "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCqbKbpMHTK3DUe\\nzZsI+Zbl1GEA5bQ6q5G+QSzBR5l/gkudioa94L+6bBeb2s6CsePBUb57mjldAM+F\\ngjwZVwjuGUhZd0mDByDWvEcz2oXfWWQs8PGrZpMxMCJPrPK6VruuO82Kc8dI4EBv\\nD7QGIT6u26+3bYeU93cGKE2onGBvVNS/67euWD9WQlztzbe8eIlOMCgpGV0E0hlg\\nPZOYwS/nwpAXo69ElXVkiI8haFzLfBG1k/G1p2eeIi7Ul2iCKupxgAbok88rbr2y\\n419cYq0sZfvtclv6S0k4yqOilDfXZl6nbwg4niEg5PJUMKMgyeE6qRWfFaHd5DFs\\nmaw4UsLFAgMBAAECggEAA1fnVfeaSvW5lKjSJYGgzFIRM+kA7Ke7nIcymN+0axp3\\nLLHwUit/FDau2q/KHlx3P3snKM1npuOI4eS74UyVg117aS0camKKg+JX5g4GIB5+\\nU+otxSZZgknTjbGZMqU+wFYyNZWifFNqucl0LcY0/YD+a2a0L4ZDtc5QaIIZqDil\\nhvB8jq69Bx4XaC1meeSbb0xsKkv+HMnR2cNWNm32F3SrVKd4jpNTihXrn0JVTq1f\\nPLD8a/o3FrPLawLLKkiD2T4jxD11eBFoomX1cTp7h/jnrrCTPlvJa3IN0ojdShSN\\n3E9J6SCqHm6MR5x6Ja2/OXztI31iVdKqvGkjR9d37wKBgQDSVRZ4PjV2EFaMtCIW\\neQ/DfUrwsnhGIdwx3s9INzFmyPrh5g3hALzSutGkxt2bB3vatjY6tm5ux3Qhw/Sk\\n3OXWFg4wVn/45R4HRz5We0/rFtA9WnmloEhGbaDvrlyM8ayCAB0Kpfz78W1uke89\\ntaLrJJCpIQdK3RlKAGDwbQuFYwKBgQDPbVvoppMGOwGDCQaN6SynpjGu8ou+GxB1\\nUUmplpxTV2A0NlLhq1lCeAsy1cMh7257Hxmip6UoNEQ3341TEk7ozLPh2C4jWJkr\\nYNWM7wazLgJKRec5ONp0syzLq67fNyea5MbYmBwrCkJZYC5KIQDo+kn/ajhEpLjf\\nXPRwflTDtwKBgBB3nvG9FRxwPQZ5F+4gfk504cgkgOwkvIOfCSWDRLfiZloHU/DY\\nyO/Ktp0rALChjl3aRDamj9DWhs4Z9G5v6QKw6V15eD15WreBKW1F7ETgnV8gVUPV\\nX7M75Ay/3vkd9r6a2as6warMBSBSBqQTM3UrgLPtJRAEbXVwwCYys2M3AoGAYaxR\\nNwAZAzoNwDIjz/cYe+kaRBMaEm6UFWotkZnGCsh+GJfr9y/EXDRCsbFOrVAdagR/\\nAcSHVKMn7FdsKM3aMm0R1ysArswtc+upIy/6q6zijy7TbnKDC6jDUzSIp7tE/v7z\\nU1JtESdHjuZnZaic0RfqeEcAL/KQyzzuLXqPV4kCgYALdJnwZ0TuA4675ms/tI2Q\\nUdv204yAFPd7bdWeq0cvLgxfmlUXXibFyTPT/7SDPjcc9NXTPuO1VwszDoY40xVZ\\n6kmPjC4UMnc0Oju7zWY+/MY1yFVS7Ig1ZnSn8YvukbERd9tceFmY6uxdID6PnZho\\nfqZD1cYlWjQIonvvRJ8g5g==\\n-----END PRIVATE KEY-----\\n\",\n" +
                    "  \"client_email\": \"firebase-adminsdk-r596w@cabbie-passenger-debug.iam.gserviceaccount.com\",\n" +
                    "  \"client_id\": \"113401647351802836510\",\n" +
                    "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                    "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                    "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                    "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-r596w%40cabbie-passenger-debug.iam.gserviceaccount.com\"\n" +
                    "}\n";
        }
    },
    STAGE {
        @Override public String environmentName() { return "stage"; }

        @Override
        public String dataBaseUrl() {
            return null;
        }

        @Override
        public String credential() {
            return null;
        }
    },
    PRODUCTION {
        @Override public String environmentName() { return "production"; }

        @Override
        public String dataBaseUrl() {
            return "https://lipo-passenger-production.firebaseio.com";
        }

        @Override
        public String credential() {
             return "{\n" +
                     "  \"type\": \"service_account\",\n" +
                     "  \"project_id\": \"lipo-passenger-production\",\n" +
                     "  \"private_key_id\": \"214bd872a88b70be1e1e7cc1f5ac841ae5b2d55f\",\n" +
                     "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC0jIOW2DZYkOyE\\na8PTOXIedJD2t+qBxJD9wyO23FnUhN6r2ffVqUUm5mYBpu5FkE15/2r5sb5UsoXW\\nuuYVbOR981R1a2ja4DHTbb5v3DdzyCmJmlAY2RFIXj74VRXaIw18Y8kJYSzk4MF4\\n3VlLNTqLFNVf+dR1zSgz3HX+FEJ6DGZvn8outTHFtItKWGMC91CmP2H+kVUved/g\\nANn1kBYvuedB8Uz/S8XizHu/NaaE5GO3kthZ0rpah/b4DhUKWuoncpN+ePFEHMrw\\nt4GHwSzk9JKoCf3wa8iHuz7+FOBhWv58HpMJeBIZBMjciVkh5GLWWnoIdpXLiHpm\\nr9XGzSnPAgMBAAECggEAGZ9fWrGbH303CPaU4eMN06NN91V+eFAXFjbyrelzmM9f\\nx8UKgPihZwsyKsXanBCwjhcLbXLQxLWufMd6uPZX2Zqwv8Ewg5oRjXnORD2yY5di\\ncwltQzl6/xQw8jkmUsDAEd5vZ19dFFY5NBflq4ta9ZCiqhjgZ/S84dnwUiXkkUFN\\nj5WFE5rvOkr5ZoBshddGN/OXDIXOmAHoJrhrMQw/So3H7zT47ZfXJU2P+di3edtQ\\ndQ0NegT4HFzWU2RPrKrPZO1W2R0HplWMhOUFmuPhaDtl2TlvAw0VNyFiIepo+eBW\\ngo1YYJSd4mL+oUmpTmoihm/Dzk4H67xr4HKDyARysQKBgQDY1BGPWH5SrW7A9vBm\\n6MxUNVmZ1NjKbsbsyDP+r2DX1a1SngE0L51Qv3XmOn5l0rOufgy/tzGuBKGksdV0\\nGfQ27IVjUuAU5mNYW+OyuXM+k9wy4Uft8TxKDzisiqPO6LvjExOynSaEbRaQUZLo\\nONbKvBp6c5+WFJlHWYB798E/CwKBgQDVKpT7lxvM/IDsGqeYGyoV1xys+83aLSbX\\n6vSkQeND9pAeLsRUHvyzIR3iXaqcIukZCc8iADz7fYcfDaYvZj/etVsAGW8croqO\\n8CrranVNPOgOeBMuWkWik9pzrdWoCvstKrrmf739Hq9q8Ty5K5r5EPT6Jd0og057\\no2e3LU3KzQKBgQCeUwuXbfifikAq6qVTn31Dp8kLSJ4cApOCZhWBTLIcJ8xtXS5n\\n2rS2N3dxaxjMZPQK38OTYcMG3B7j0W6ZXjIywLYNdoBlAm1cIYy7/iomcXUCOfl5\\nxwJM2Zx6cKl39fzrrAxgCdwMKlOlgcsRgh9k3cE/1JmczuG1dacZ3ijfTQKBgHmh\\noX7pWyidD6cpEmYdYecyZh9iUrRiPR7I5yzul5IJF3TRdJ8XS1Oe2uF2VspetSjf\\nSvx4rFNH7hjO/ftf2aPnUXR+DsOL6G3lJmhLx4D59uOCqlcxlThOK0EEeLw4/9dm\\n4VxymWLb5nYcDm0OL5Bcl5xAjK/4NIkh1SVVSWUlAoGBAI1xS2nj6d0W+QH4pwXV\\nkIDmLlSdZS9AbQHjFmvsv5UpCZ4fuV8940BOEjHvnx5/uGSErfr0XBrdqzsoVkfd\\nJdqzZ8G37XuD0PgWU6/rYdeL3uHDAvNsi5yeRYDbDAH40HLH7GdhMmM7+im2at+F\\ncTi0QC4KjMKoAMyEDncNIcao\\n-----END PRIVATE KEY-----\\n\",\n" +
                     "  \"client_email\": \"firebase-adminsdk-3fcuv@lipo-passenger-production.iam.gserviceaccount.com\",\n" +
                     "  \"client_id\": \"112277250981298476840\",\n" +
                     "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                     "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                     "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                     "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-3fcuv%40lipo-passenger-production.iam.gserviceaccount.com\"\n" +
                     "}\n";
        }
    };

    public static final String PASSENGER_FIREBASE_PROJECT  = "passenger";

    public abstract String environmentName();
    public abstract String dataBaseUrl();
    public abstract String credential();

    public static FirebaseCredential environment(String methodArn) {
        for (FirebaseCredential credential : FirebaseCredential.values()) {
            if(methodArn.contains(credential.environmentName())) {
                return credential;
            }
        }

        throw new NullPointerException("Cant get environment credentials");
    }
}
