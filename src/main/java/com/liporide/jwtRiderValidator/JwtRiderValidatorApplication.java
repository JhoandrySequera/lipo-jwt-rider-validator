package com.liporide.jwtRiderValidator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.liporide.jwtRiderValidator.model.AuthorizerResponse;
import com.liporide.jwtRiderValidator.model.PolicyDocument;
import com.liporide.jwtRiderValidator.model.Statement;
import com.liporide.jwtRiderValidator.model.TokenAuthorizerContext;
import com.liporide.jwtRiderValidator.util.JWTOptions;

import java.util.Collections;
import java.util.logging.Logger;

public class JwtRiderValidatorApplication implements RequestHandler<TokenAuthorizerContext, AuthorizerResponse> {

	private final static Logger LOGGER = Logger.getLogger(JwtRiderValidatorApplication.class.getName());

	@Override
	public AuthorizerResponse handleRequest(TokenAuthorizerContext request, Context context) {
		Statement statement = getStatement(request.getMethodArn(), "Deny");

		try {
			JWTOptions.validate(request.getAuthorizationToken(), request.getMethodArn());
			statement = getStatement(request.getMethodArn(), "Allow");
		} catch (Exception e) {
			LOGGER.warning("ERROR validating JWT -> " + e.getMessage());
		}

		return AuthorizerResponse.builder()
				.principalId("passenger")
				.policyDocument(
						PolicyDocument.builder()
								.statements(Collections.singletonList(statement))
								.build())
				.build();
	}

	private Statement getStatement(String arn, String effect) {
		return Statement.builder()
				.resource(arn)
				.effect(effect)
				.build();
	}
}
